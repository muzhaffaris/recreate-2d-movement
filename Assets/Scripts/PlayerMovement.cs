using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [Header("Movement Speed")]
    public float walkSpeed = 7f;
    public float ladderClimbSpeed = 7f;
    public float jumpVelocity = 7f;
    public float fallMultiplier = 2.5f;
    public float lowJumpMultiplier = 2f;

    public LayerMask groundLayerMask;
    public LayerMask ladderLayerMask;

    private Rigidbody2D rb2d;
    private BoxCollider2D boxCollider2D;

    private bool isClimbing;

    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        boxCollider2D = GetComponent<BoxCollider2D>();
    }


    private void Update()
    {
        //lompat
        if (Input.GetButtonDown("Jump") && IsGrounded())
        {
            rb2d.velocity = Vector2.up * jumpVelocity;
        }

        if (rb2d.velocity.y < 0)
        {
            rb2d.velocity += Vector2.up * Physics2D.gravity.y * (fallMultiplier - 1);
        }
        else if (rb2d.velocity.y > 0 && !Input.GetButton("Jump"))
        {
            rb2d.velocity += Vector2.up * Physics2D.gravity.y * (lowJumpMultiplier - 1);
        }


        //panjat tangga
        float ladderDistance = 0.5f;
        RaycastHit2D ladderHit = Physics2D.Raycast(transform.position, Vector2.up, ladderDistance, ladderLayerMask);

        if (ladderHit.collider != null)
        {
            if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.W))
            {
                isClimbing = true;
            }
        }
        else
        {
            isClimbing = false;
        }

        if (isClimbing)
        {
            float verticalInput = Input.GetAxisRaw("Vertical");
            rb2d.velocity = new Vector2(rb2d.velocity.x, verticalInput * ladderClimbSpeed);
            rb2d.gravityScale = 0;
        }
        else
        {
            rb2d.gravityScale = 1;
        }
    }

    private void FixedUpdate()
    {
        //jalan kanan kiri
        var movement = Input.GetAxisRaw("Horizontal");
        rb2d.velocity = new Vector2(movement * walkSpeed, rb2d.velocity.y);

    }

    private bool IsGrounded()
    {
        float extraHeightTest = 0.2f;
        RaycastHit2D raycastHit = Physics2D.BoxCast(boxCollider2D.bounds.center, boxCollider2D.bounds.size, 0f, Vector2.down, extraHeightTest, groundLayerMask);

        //debug
        /*
        Color rayColor;
        if (raycastHit.collider != null)
        {
            rayColor = Color.green;
        }
        else
        {
            rayColor = Color.red;
        }
        Debug.DrawRay(boxCollider2D.bounds.center + new Vector3(boxCollider2D.bounds.extents.x, 0), Vector2.down * (boxCollider2D.bounds.extents.y + extraHeightTest), rayColor);
        Debug.DrawRay(boxCollider2D.bounds.center - new Vector3(boxCollider2D.bounds.extents.x, 0), Vector2.down * (boxCollider2D.bounds.extents.y + extraHeightTest), rayColor);
        Debug.DrawRay(boxCollider2D.bounds.center - new Vector3(boxCollider2D.bounds.extents.x, boxCollider2D.bounds.extents.y + extraHeightTest), Vector2.right * (boxCollider2D.bounds.extents.x), rayColor);
        Debug.Log(raycastHit.collider);
        */

        return raycastHit.collider != null;
    }
}
